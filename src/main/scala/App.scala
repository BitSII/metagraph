import java.util.concurrent.TimeUnit

import actor.ComponentActor._
import actor.CounterActor.{Get, Inc}
import actor.VertexActor._
import actor.{CounterActor, MetagraphActor, VertexActor}
import akka.actor.{ActorRef, ActorSystem, Props}
import akka.pattern.AskableActorRef
import akka.util.Timeout
import components.{Component, MetaVertex, Vertex}
import components.implicits.MetaVertexImplicits._

import scala.concurrent.duration.FiniteDuration
import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.{Failure, Random, Success}

object App {

  def main(args: Array[String]): Unit = {
    implicit val timeout = Timeout(FiniteDuration(1, TimeUnit.SECONDS))

    //    val v1 = new Vertex("v1")
//    val v2 = new Vertex("v2")
//    val v3 = new Vertex("v3")
//    val v4 = new Vertex("v4")
//    val v5 = new Vertex("v5")
//    val v6 = new Vertex("v6")
//    val v7 = new Vertex("v7")
//
//    val e1 = v1 ++ v2
//    val e2 = v2 ++ v3
//    val e3 = v1 ++ v3
//    val mv1 = MetaVertex.+("mv1", v1, v2, v3, e1, e2, e3)
//
//    val e4 = v2 ++ v4
//    val e5 = v3 ++ v5
//    val e6 = v4 ++ v5
//    val mv2 = MetaVertex.+("mv2", v4, v5, e6)
//
//    val e7 = mv1 ++ mv2
//    val e8 = mv2 ++ v2
//    val mv4 = MetaVertex.+("mv4", v6, v7)
//
//    val mv3 = MetaVertex.+("mv3", v2, v3, e4, e5, e8, mv2, mv4)

//    val fullMetagraph = MetaVertex.+("full", mv1, mv3, e7)
//    val full2: MetaVertex = v1 + v2 + mv2 + mv4 + e4
//    println(full2)
    val system = ActorSystem("testSystem")
    val root: AskableActorRef = new AskableActorRef(system.actorOf(VertexActor.props, "root"))
    val counter = system.actorOf(Props(new CounterActor), "counter")
//    root ? MsgCreateChildVertex("v1", None, Random.alphanumeric.take(10).mkString)
//    root ? MsgCreateChildVertex("v2", None, Random.alphanumeric.take(10).mkString)
//    root ? MsgCreateChildVertex("v3", None, Random.alphanumeric.take(10).mkString)

    for (i <- 1 to 20000) {
      system.actorSelection("user/" + "counter").resolveOne().onComplete {
        case Success(actorRef) => {
          actorRef ! Inc()
        }
      }
      if (i < 50) {
        root ? VertexActor.MsgCreateSubVertex(i.toString, "root", i.toString)
      } else {
        val rnd = Math.abs(Random.nextInt() % 8)
        if (rnd == 0) {
          root ? VertexActor.MsgCreateSubVertex(i.toString, "root", i.toString)
        } else {
          var rndParent = Math.abs(Random.nextInt() % i)
          if (rndParent == 0) rndParent += 1
          if (rndParent - i < 50) rndParent = i - 50
//          println(s"rndParent = $rndParent")
          root ? MsgCreateSubVertex(i.toString, rndParent.toString, i.toString)
        }
      }
      if (i % 100 == 0) {
        system.actorSelection("user/" + "counter").resolveOne().onComplete {
          case Success(actorRef) => {
            (new AskableActorRef(actorRef) ? Get()).onComplete(tryAny => println(tryAny.get.asInstanceOf[Int]))
          }
          case Failure(ex) => println("noo")
        }
      }
      Thread.sleep(20)
    }

    Thread.sleep(1000)
    system.actorSelection("user/" + "counter").resolveOne().onComplete {
      case Success(actorRef) => {
        (new AskableActorRef(actorRef) ? Get()).onComplete(tryAny => println("last" + tryAny))
      }
      case Failure(ex) => println("noo")
    }
    root ? MsgDump()
  }
}
