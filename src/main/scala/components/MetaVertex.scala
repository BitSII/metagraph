package components

class MetaVertex(name: String, private val metaGraph: MetaGraph) extends Vertex(name) {

  override def clone(): AnyRef = {
    new MetaVertex(name, new MetaGraph(metaGraph.vertices.clone(), metaGraph.edges.clone()))
  }

  override def dump(): String = super.dump + '\n' + metaGraph.dump

  override def contains(vertex: Vertex): Boolean = {
    metaGraph.contains(vertex)
  }

  //возвращает текущую вершину со всеми вложенными в виде Сета
  override def toSet(): Set[Vertex] = super.toSet() ++ descendantVertices()

  //удаление
  override def -(removingVertex: Vertex): MetaVertex = {
    if (!metaGraph.vertices.contains(removingVertex)) {
      return clone().asInstanceOf[MetaVertex]
    }

    if (!metaGraph.vertices.exists(v => v.isInstanceOf[MetaVertex] && v.contains(removingVertex))) {
      val newMv = clone().asInstanceOf[MetaVertex]
      newMv.metaGraph.vertices.remove(removingVertex)
      newMv
    } else {
      throw new IllegalAccessException("Нарушение целостности, удаляемая вершина входит во вложенную метавершину")
    }
  }

  //транзитивное удаление
  def *- (removingVertex: Vertex): MetaVertex = {
    val newMv = clone().asInstanceOf[MetaVertex]

    if (!descendantVertices().contains(removingVertex)) {
      return newMv
    }

    newMv.metaGraph.transitiveRemove(removingVertex)
    newMv
  }

  //количество дочерних вершин(1 уровень вложенности)
  def countChilds = metaGraph.vertices.size

  //количество всех вложенных вершин на любом уровне
  def countDescendants = descendantVertices().size

  //возвращает все вложенные вершины в виде Сета
  def descendantVertices(): Set[Vertex] = metaGraph.allVertices()
}

object MetaVertex {
  def + (name: String, components: Component*): MetaVertex = {
    val mg = new MetaGraph()
    components.foreach {
      case v: Vertex =>
        mg.vertices.add(v)
      case e: Edge =>
        mg.edges.add(e)
    }
    new MetaVertex(name,mg)
  }
}
