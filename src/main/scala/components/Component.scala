package components

trait Component extends Cloneable {
  def dump(): String
  def +(component: Component): MetaGraph
}
