package components

import scala.collection.mutable

class Edge(val startVertex: Vertex, val endVertex: Vertex, isOriented: Boolean = false) extends Component {
  val attrs = new mutable.HashMap[String, Any]()

  override def dump(): String = startVertex.name + " - " + endVertex.name

  override def +(component: Component): MetaGraph = {
    val mg = new MetaGraph()
    mg.edges.add(this)
    component match {
      case v: Vertex => mg.vertices.add(v)
      case e: Edge => mg.edges.add(e)
    }
    mg
  }
}
