package components

import scala.collection.mutable

class Vertex(val name: String) extends Component {
  val attrs: mutable.HashMap[String, Any] = new mutable.HashMap()

  def contains(vertex: Vertex): Boolean = {
    vertex == this
  }

  def toSet() = {
    Set(this)
  }

  def ++(endVertex: Vertex): Edge = {
    new Edge(this, endVertex)
  }

  override def + (component: Component): MetaGraph = {
    val mg = new MetaGraph()
    mg.vertices.add(this)
    component match {
      case v: Vertex => mg.vertices.add(v)
      case e: Edge => mg.edges.add(e)
    }
    mg
  }

  //обычная вершина - атомарный элемент, нельзя ничего удалить
  def -(removingVertex: Vertex): Vertex = {
    this
  }

  override def dump(): String = {
    name
  }
}
