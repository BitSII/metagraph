package components

import scala.collection.mutable

class MetaGraph(
                 val vertices: mutable.Set[Vertex] = new mutable.HashSet(),
                 val edges: mutable.Set[Edge] = new mutable.HashSet ()
               ) {

  def +(component: Component): MetaGraph = {
    component match {
      case e: Edge =>
        edges.add(e)
      case v: Vertex =>
        vertices.add(v)
    }
    this
  }

  def contains(vertex: Vertex): Boolean = {
    vertices.contains(vertex) || vertices.exists(_.contains(vertex))
  }

  def allVertices(): Set[Vertex] = {
    vertices.flatMap(v => v.toSet()).toSet
  }

  //удаляет все метавершины, которые содержат конкретную вершину
  def transitiveRemove(vertex: Vertex): Unit = {
    vertices --= vertices.filter(_.contains(vertex))
  }

  def dump: String = {
    val sb = new StringBuilder()
    sb.append("[\n")
        .append(edges.map(_.dump()).mkString(", "))
    if (edges.nonEmpty) {
      sb.append('\n')
    }
    sb.append(vertices.map(_.dump()).mkString(",\n"))
      .append("\n]")
    sb.toString()
  }
}
