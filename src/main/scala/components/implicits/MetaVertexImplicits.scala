package components.implicits

import components.{Component, MetaGraph, MetaVertex}

object MetaVertexImplicits {
  implicit def metaGraphToMetaVertex(metaGraph: MetaGraph): MetaVertex = {
    new MetaVertex("", metaGraph)
  }
}
