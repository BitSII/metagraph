package actor

import actor.ComponentActor.{MsgResultRef, MsgSearchByName}
import actor.CounterActor.{Dec, Inc}
import actor.VertexActor.{MsgCreateChildVertex, MsgCreateMetaVertex, MsgCreateSubVertex, MsgRemoveVertex}
import akka.actor.{ActorRef, Props}
import akka.pattern.AskableActorRef

import scala.collection.mutable
import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.Success

class VertexActor extends ComponentActor {
  //если массив вложенных вершин не пустой - вершина является метавершиной
  private val vertices: mutable.Set[AskableActorRef] = new mutable.HashSet()
  //массив ребер, соединенных с вершиной
  private val edges: mutable.Set[AskableActorRef] = new mutable.HashSet()


  // в первую очередь обрабатываются типы сообщения, подходящие для ComponentActor, после этого - сообщения, специфичные для вершины
  override def receive: Receive = super.receive.orElse {
    //создание дочерней вершины в иерархии akka
    case MsgCreateChildVertex(name, parent, operationCode) =>
      parent match {
        // обработка варианта, когда в полученном сообщении присутствует имя родителя
        case Some(parentName) =>
          //если имя родителя совпадает с именем текущей вершины, создается дочерняя вершина
          if (context.self.path.name == parentName) {
            //если уже существует вершина с таким именем, вторая не создается
            //данное условие гарантирует уникальность имен дочерних вершин
            if (!context.children.map(_.path.name).toSet.contains(name)) {
              context.actorOf(VertexActor.props, name)
            }
          } else {
            //в противном случае сообщение о создании рассылается всем потомкам
            context.children.foreach { it =>
              it ! MsgCreateChildVertex(name, parent, operationCode)
            }
          }

        // обработка варианта, когда в полученном сообщении ет имени родителя
        // создание дочерней вершины у текущей
        case None =>
          context.actorOf(VertexActor.props, name)
      }

    //создание новой вложенной вершины
    case MsgCreateSubVertex(name, parentName: String, operationCode: String) =>
//      context.system.actorSelection("user/" + "counter").resolveOne().onComplete {
//        case Success(actorRef) => {
//          actorRef ! Dec()
//        }
//        case scala.util.Failure(exception) => println("fail")
//      }
      if (ifOperationNotExecuted(operationCode)) {
//        println(s"($parentName) (${self.path.name})")
        if (parentName == self.path.name) {
//          println(s"adding $name as inherit vertex of ${self.path.name}")
          vertices.add(context.system.actorOf(VertexActor.props, name))
        } else {
          sendMsgBroadcast(MsgCreateSubVertex(name, parentName, operationCode), sender())
        }
      }

    //замена оператора "+" метаграфового исчисления. Создание метавершины на основе двух вложенных вершин
//    case MsgCreateMetaVertex(secondVertex, name) =>
//      val v = context.system.actorOf(VertexActor.props, name) //Создание метавершины в корне акторной системы
//      //добавление вложенных вершин
//      v ! MsgCreateSubVertex(self)
//      v ! MsgCreateSubVertex(secondVertex)
//      sender() ! v

    //операция удаления вершины-актора
    case MsgRemoveVertex(name, operationCode) =>

      //если имя удаляемого актора совпадает с текущим, останавлимается текущий актор
      if (name == self.path.name) {
        context.stop(self)
      } else {
        //поиск актора среди потомков и его удаление
        context.child(name) match {
          case Some(ref) =>
            context.stop(ref)
        }
      }
  }


  override def dump(): Unit = {
    println(s"=")
    println(s"${vertices.map(_.actorRef.path.name)}")
    println(s"${edges.map(_.actorRef.path.name)}")
    println(s"=====")
  }

  override def postStop(): Unit = {
    super.postStop()
    //при остановке (удалении) актора-вершины необходимо удалить все ребра, связывающие вершину с другими вершинами
    edges.foreach(edge => context.stop(edge.actorRef))
  }

  // при поиске элемента в вершине осуществляется поиск по всем вложенным вершинам и соединенным ребрам
  override def searchByName(name: String, operationCode: String): Future[Option[ActorRef]] = {
    //всем вершинам и всем ребрам отправляется сообщение на поиск элемента
    //таким образом, в каждом элементе поиск осуществляется параллельно
    Future.sequence(
      //объединение результатов поиска по всем вершинам и всем ребрам
      vertices.map { v =>
        v ? MsgSearchByName(name, operationCode) // создание объекта типа Future, который завершится при получении ответа на отправленное сообщение
      } ++ edges.map { v =>
        v ? MsgSearchByName(name, operationCode)
      }
    ).map { results =>
      //после завершения поиска по всем элементам берется первый успешный непустой результат, если таковой имеется
      results.flatMap {
        case r: MsgResultRef =>
          r.result
        case _ => None
      }.headOption
    }
  }

  override protected def sendMsgBroadcast(msg: Any, sender: ActorRef): Unit = {
    vertices.filter(_.actorRef.path.name != sender.path.name).foreach { v =>
//      println(s"broadcast $msg to ${v.actorRef.path.name}")
      v.actorRef ! msg
      context.system.actorSelection("user/" + "counter").resolveOne().onComplete {
        case Success(actorRef) => {
          actorRef ! Inc()
        }
        case scala.util.Failure(exception) => println("fail")
      }
    }
//    println(VertexActor.counter)
    edges.filter(_.actorRef.path.address != sender.path.address).foreach { e =>
      e.actorRef ! msg
    }
  }
}

object VertexActor {
//  var counter = 0
//  val monitor = new Any()
//  def incr() = {
//    monitor.synchronized {
//
//    }
//  }

  def props: Props = Props(new VertexActor)

  case class MsgCreateChildVertex(
    name: String,
    parent: Option[String] = None,
    operationCode: String
  )
  case class MsgCreateSubVertex(name: String, parentName: String, operationCode: String)
  case class MsgCreateMetaVertex(
    secondVertex: ActorRef,
    name: String
  )
  case class MsgRemoveVertex(
    name: String,
    operationCode: String
  )
  case class MsgCreateChildVertexAsync(
    name: String,
    parent: Option[String] = None,
    operationCode: String
  )
  case class MsgRemoveVertexAsync(
    name: String,
    operationCode: String
  )
}
