package actor

import java.util.concurrent.TimeUnit

import actor.ComponentActor._
import akka.actor.{Actor, ActorRef}
import akka.util.Timeout

import scala.collection.mutable
import scala.concurrent.Future
import scala.concurrent.duration.{Duration, FiniteDuration}
import scala.util.{Failure, Success}
import scala.concurrent.ExecutionContext.Implicits.global

trait ComponentActor extends Actor {
  implicit val timeout = Timeout(FiniteDuration(1, TimeUnit.SECONDS))

  //набор аттрибутов элемента
  private val attrs: mutable.Map[String, Any] = new mutable.HashMap()
  //учет предыдущих операций, необходим, чтобы избежать бесконечных циклов в операциях с обходом метаграфа (поиск)
  private val previousOperations = new mutable.HashSet[String]()

  protected def searchByName(name: String, operationCode: String): Future[Option[ActorRef]]
  protected def sendMsgBroadcast(msg: Any, sender: ActorRef): Unit
  def dump() = {}

  override def receive: Receive = {
    case MsgDump() =>
      println(s"dumping ${context.self.path.name}: ${context.self.path.address} ops:${previousOperations.toSeq}")
      dump()
      Thread.sleep(10)
      sendMsgBroadcast(MsgDump(), sender())

    // добавление нового аттрибута элементу метаграфа
    case MsgAddAttr(componentName, name, value, operationCode) =>
      if (ifOperationNotExecuted(operationCode))  {
        attrs.put(name, value)
      }

    // поиск элемента метаграфа по имени
    case MsgSearchByName(name: String, operationCode: String) =>
      val s = sender()
      //если данная операция уже выполнялась на текущем элементе,
      //необходимо ответить пустым результатом и не продолжать поиск
      //во избежании вечных циклов
      if (previousOperations.contains(operationCode)) {
        s ! MsgResultRef(None)
      } else if (context.self.path.name == name) { // проверка - является ли текущая вершина искомой
        previousOperations.add(operationCode) //добавление операции в массив полняемых на данном элементе
        // если вершина является, отправляем в ответе ссылку на текущую вершину
        s ! MsgResultRef(Some(self))
      } else {
        previousOperations.add(operationCode) //добавление операции в массив полняемых на данном элементе
        // если не является, ищем текущую вершину во вложденных компонентах
        // реализация функции поиска различается у различных компонентов
        searchByName(name, operationCode).onComplete {
          // в случае успешного поиска результат отсылается вершине-отправителю
          case Success(result) =>
            result match {
              case Some(_) =>
                //если что-то найдено, отправляется результат
                s ! MsgResultRef(result)
              case _ =>
                //если получено сообщение другого типа, отправляется сообщение о том, что ничего не было найдено
                s ! MsgResultRef(None)
            }

          // в случае ошибки производится печать информации в терминал
          case Failure(e) =>
            e.printStackTrace()
        }
      }
  }

  protected def ifOperationNotExecuted(operationCode: String): Boolean = {
    if (!previousOperations.contains(operationCode)) {
      previousOperations.add(operationCode)
      true
    } else  {
      false
    }
  }


}

object ComponentActor {
  case class MsgDump()
  case class MsgResult(result: Any)
  case class MsgResultRef(result: Option[ActorRef])

  case class MsgLog()
  case class MsgAddAttr(
    componentName: String,
    name: String,
    value: Any,
    operationCode: String
  )
  case class MsgRemoveAttr(
    componentName: String,
    name: String,
    operationCode: String
  )
  case class MsgSearchByName(
    name: String,
    operationCode: String
  )
  case class MsgSearchByAttr(
    attrName: String,
    attrValue: Any,
    operationCode: String
  )
}