package actor

import actor.CounterActor.{Dec, Get, Inc}
import akka.actor.Actor

class CounterActor extends Actor {
  var counter = 0
  override def receive: Receive = {
    case Inc() =>
      counter += 1
    case Dec() =>
      counter -= 1
    case Get() =>
      sender() ! counter
      counter = 0
  }
}

object CounterActor {
  case class Inc()
  case class Dec()
  case class Get()
}
