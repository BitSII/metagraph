package actor

import akka.actor.{Actor, ActorRef, ActorSystem, Props}

import scala.concurrent.Future

class MetagraphActor extends ComponentActor {
  override def receive: Receive = super.receive

  override protected def searchByName(name: String, operationCode: String): Future[Option[ActorRef]] = {
    Future.successful(None)
  }

  override protected def sendMsgBroadcast(msg: Any, sender: ActorRef): Unit = {}
}

object MetagraphActor {
  def props: Props = Props(new MetagraphActor)

  def create(system: ActorSystem): ActorRef = {
    system.actorOf(props, "metagraph")
  }
}
