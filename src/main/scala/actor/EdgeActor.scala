package actor

import actor.ComponentActor.{MsgResult, MsgResultRef, MsgSearchByName}
import actor.EdgeActor.MsgGetVertices
import akka.actor.{ActorRef, Props}
import akka.pattern.AskableActorRef

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future


class EdgeActor(val startVertex: AskableActorRef, val endVertex: AskableActorRef) extends ComponentActor {


  override def receive: Receive = super.receive.orElse {
    case MsgGetVertices() =>
      sender() ! MsgResult((startVertex, endVertex))
  }

  //при поиске элемента по ребру происходит проксирование запроса на вершину, которую ребро соединяет
  override def searchByName(name: String, operationCode: String): Future[Option[ActorRef]] = {
    val s = sender()
    // сообщение отправляется на вершину, противоположную той, от которой пришел запрос
    if (s.path.address == startVertex.actorRef.path.address) {
      return endVertex ? MsgSearchByName(name, operationCode) map {
        case MsgResultRef(ref) =>
          ref
        case _ =>
          None
      }
    }
    if (s.path.address == endVertex.actorRef.path.address) {
      return startVertex ? MsgSearchByName(name, operationCode) map {
        case MsgResultRef(ref) =>
          ref
        case _ =>
          None
      }
    }
    Future.successful(None)
  }

  override protected def sendMsgBroadcast(msg: Any, sender: ActorRef): Unit = {
    if (sender.path.address == startVertex.actorRef.path.address) {
      endVertex.actorRef ! msg
    }

    if (sender.path.address == endVertex.actorRef.path.address) {
      startVertex.actorRef ! msg
    }
  }
}


object EdgeActor {
  def props: Props = Props(new VertexActor)

  case class MsgGetVertices()
}