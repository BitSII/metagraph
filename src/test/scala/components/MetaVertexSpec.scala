package components
import org.scalatest.{FreeSpec, Matchers}

class MetaVertexSpec extends FreeSpec with Matchers {
  "MetaVertex should" - {
    val v1 = new Vertex("v1")
    val v2 = new Vertex("v2")
    val v3 = new Vertex("v3")
    val v4 = new Vertex("v4")
    val mv1 = MetaVertex.+("mv1", v1, v2)
    val mv2 = MetaVertex.+("mv2", v2, v3, mv1)
//    val testMetaVertex = initMetaVertex()
    "check count" - {
      "child vertices" in {
        mv1.countChilds shouldEqual 2
        mv2.countChilds shouldEqual 3
      }

      "descendant verticles" in {
        mv1.countDescendants shouldEqual 2
        mv2.countDescendants shouldEqual 4
      }

      "of empty" in {
        val emptyMg = new MetaVertex("empty", new MetaGraph())
        emptyMg.countChilds shouldEqual 0
        emptyMg.countDescendants shouldEqual 0
      }
    }

    "check contains" - {
      "child vertex" in {
        mv2.contains(v2) shouldEqual true
        mv2.contains(v4) shouldEqual false
      }

      "deep child vertex" in {
        mv2.contains(v1) shouldEqual true
      }

      "other metaVertex" in {
        mv2.contains(mv1) shouldEqual true
      }
    }

    "get vertices" - {
      "from toSet method" in {
        val vertices = mv2.toSet()
        vertices.contains(v1) shouldEqual true
        vertices.contains(v2) shouldEqual true
        vertices.contains(v3) shouldEqual true
        vertices.contains(mv1) shouldEqual true
        vertices.contains(mv2) shouldEqual true
        vertices.contains(v4) shouldEqual false
      }

      "from descendantVertices" in {
        val vertices = mv2.descendantVertices()
        vertices.contains(v1) shouldEqual true
        vertices.contains(v2) shouldEqual true
        vertices.contains(v3) shouldEqual true
        vertices.contains(mv1) shouldEqual true
        vertices.contains(mv2) shouldEqual false
        vertices.contains(v4) shouldEqual false
      }
    }

    "remove" - {
      "child vertex" in {
        val count = mv2.countDescendants
        val newMv = mv2 - v3
        count - newMv.countDescendants shouldEqual 1
        newMv.contains(v3) shouldEqual false
        newMv.contains(v2) shouldEqual true
        newMv.contains(v1) shouldEqual true
        newMv.contains(mv1) shouldEqual true
      }

      "without side-effects for operators" in {
        val mv2Count = mv2.countDescendants
        val mv1Count = mv1.countDescendants

        val newMv = mv2 - mv1
        mv2.countChilds - newMv.countChilds shouldEqual 1
        mv2.countDescendants shouldEqual mv2Count
        mv1.countDescendants shouldEqual mv1Count
      }

      "not existing vertex" in {
        mv2.countDescendants shouldEqual  (mv2 - v4).countDescendants
      }

      "thrown exception when remove" in {
        an [IllegalAccessException] should be thrownBy mv2 - v2
      }
    }

    "transitive remove" - {
      "child vertex" in {
        val newMv = mv2 *- v3
        newMv.contains(v2) shouldEqual true
        newMv.contains(mv1) shouldEqual true
        newMv.contains(v3) shouldEqual false
      }


      "child and descendant vertex" in {
        val newMv = mv2 *- v2
        newMv.contains(v2) shouldEqual false
        newMv.contains(mv1) shouldEqual false
        newMv.contains(v3) shouldEqual true
      }

      "without side-effects for operators" in {
        val mv2Count = mv2.countDescendants
        val mv1Count = mv1.countDescendants
        val newMv = mv2 *- v2
        mv2.countDescendants shouldEqual mv2Count
        mv1.countDescendants shouldEqual mv1Count
        newMv.countDescendants should be < mv2Count
      }

      "not existing vertex" in {
        val newMv = mv2 *- v4
        newMv.countDescendants shouldEqual mv2.countDescendants
      }
    }
  }

//  def testMetaVertex: MetaVertex = {
//    val v1 = new Vertex("v1")
//    val v2 = new Vertex("v2")
//    val v3 = new Vertex("v3")
//    val mv1 = MetaVertex.+("mv1", v1, v2)
//    MetaVertex.+("mv2", v2, v3, mv1)
//  }
}
